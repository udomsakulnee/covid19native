import { SEND_TODO, CLEAR_TODO } from "../constants";

const initialState = {
    status: false,
    data: {}
}

export default (state = initialState, { type, payload }) => {
    switch (type) {
        case SEND_TODO:
            return { ...state, status: true, data: payload };

        case CLEAR_TODO:
            return { ...state, status: false, data: {} }
        
        default:
            return state;
    }
}