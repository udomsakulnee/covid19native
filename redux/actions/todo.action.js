import { SEND_TODO, CLEAR_TODO } from "../constants";

export const sendTodo = payload => ({
    type: SEND_TODO,
    payload: payload
});

export const clearTodo = () => ({
    type: CLEAR_TODO
});