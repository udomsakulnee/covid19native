import React, { Component } from "react";
import { View, Button, Text } from "react-native";

export default class Toggle extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isToggleOn: true,
    };

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState({ isToggleOn: !this.state.isToggleOn });
  };

  render() {
    return (
      <View>
        <Button title="Click" onPress={this.handleClick} />
        <Text>{this.state.isToggleOn ? "ON" : "OFF"}</Text>
      </View>
    );
  }
}
