import React, { useState } from "react";
import { Button, View, Text, TextInput } from "react-native";
import { useDispatch } from "react-redux";
import { sendTodo, clearTodo } from "../../redux/actions/todo.action";

const TodoForm = () => {
  const [todo, setTodo] = useState({ title: "", description: "" });
  const dispatch = useDispatch();

  const handleSubmit = () => {
    const { title, description } = todo;
    if (title != "" && description != "") {
      dispatch(sendTodo(todo));
      setTodo({ title: "", description: "" });

      // dispatch(clearTodo());
    }
  };

  return (
    <>
      <View>
        <Text>title</Text>
        <TextInput
          style={{ height: 40 }}
          placeholder="Type here to translate!"
          onChangeText={(text) => {
            setTodo({ ...todo, title: text });
          }}
          defaultValue={todo.title}
        />
        <Text>description</Text>
        <TextInput
          style={{ height: 40 }}
          placeholder="Type here to translate!"
          onChangeText={(text) => {
            setTodo({ ...todo, description: text });
          }}
          defaultValue={todo.description}
        />
        <Button
          onPress={handleSubmit}
          title="submit"
          color="#841584"
          accessibilityLabel="Learn more about this purple button"
        />
      </View>
    </>
  );
};
export default TodoForm;
