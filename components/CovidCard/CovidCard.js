import React from "react";
import { Box, BoxTitle, BoxAmount, BoxIncrease } from "../../styles/box";
import { numberFormat } from "@wiseidea/util-format";

const CovidCard = ({ navigation, title, amount, increase, color }) => (
  // <TouchableOpacity style={{ flex: 1 }}
  //   onPress={() => navigation.push("Todo")}
  // >
    <Box color={color} onPress={() => navigation.push("Param", { title, amount, increase })}>
      <BoxTitle>{title}</BoxTitle>
      <BoxAmount>{numberFormat(amount)}</BoxAmount>
      <BoxIncrease>({increase < 0 ? "ลดลง" : "เพิ่มขึ้น"} {Math.abs(increase)})</BoxIncrease>
    </Box>
  // </TouchableOpacity>

);
export default CovidCard;
