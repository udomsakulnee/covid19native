import React, { useEffect, useState } from "react";
import { View, Text, FlatList, StyleSheet, ScrollView } from "react-native";
import { useSelector } from "react-redux";
import Constants from "expo-constants";

function Item({ title, description }) {
  return (
    <View style={styles.item}>
      <Text style={styles.title}>{title}</Text>
      <Text style={styles.title}>{description}</Text>
    </View>
  );
}

let todoList = [];
const TodoList = () => {
  // const [todoList, setTodoList] = useState([]);

  // useEffect(()=>{
  //  if(todo) {
  //   setTodoList([...todoList, todo]);

  //   console.log("[...todoList, todo] : ", [...todoList, todo]);

  //  }

  // },[todo])

  const { status, data } = useSelector((state) => state.todoReducer);
  if (status) {
    todoList = [...todoList, data];
  }

  return (
    <>
      <View>
        <Text>Show Todo List</Text>
        {/* <Text>{todo.title}</Text>
  <Text>{todo.description}</Text> */}
        {/* <ListView
        dataSource={this.state.dataSource}
        renderRow={(rowData) => <Text>{rowData}</Text>}
      /> */}
        <ScrollView>
          <FlatList
            data={todoList}
            renderItem={({ item }) => (
              <Item title={item.title} description={item.description} />
            )}
          />
        </ScrollView>
      </View>
    </>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: Constants.statusBarHeight,
  },
  item: {
    backgroundColor: "#f9c2ff",
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
});

export default TodoList;
