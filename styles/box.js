import styled from "styled-components";

export const BoxGroup = styled.View`
  flex-direction: row; 
`;

export const Box = styled.TouchableOpacity`
  border-radius: 10px;
  margin: 5px;
  background-color: ${({ color }) => (color ? color : "black")};
  padding: 10px;
  flex: 1;
`;

export const BoxTitle = styled.Text`
  text-align: center;
  color: white;
  font-size: 16px;
`;

export const BoxAmount = styled.Text`
  text-align: center;
  color: white;
  font-size: 22px;
  font-weight: bold;
`;

export const BoxIncrease = styled.Text`
  text-align: center;
  color: white;
  font-size: 12px;
`;



