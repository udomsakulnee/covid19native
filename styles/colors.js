const Colors = {
    primary: "#e1298e",
    info: "#179c9b",
    success: "#006738",
    warning: "#ffff00",
    danger: "#ff0000",
    default: "#404040",
    white: "#ffffff",
    gray: "#666666"
};
export default Colors;