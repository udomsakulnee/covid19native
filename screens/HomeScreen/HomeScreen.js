import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  Button,
  Linking,
} from "react-native";
import { BoxGroup } from "../../styles/box";
import Colors from "../../styles/colors";
import CovidCard from "../../components/CovidCard/CovidCard";
// import { todayService } from "../../services/stat-covid-19";
import { TodayService } from "../../services";
import Toggle from "../../components/Toggle/Toggle";

const HomeScreen = ({ navigation }) => {
  const [data, setData] = useState({
    Confirmed: 0,
    Deaths: 0,
    Hospitalized: 0,
    NewConfirmed: 0,
    NewDeaths: 0,
    NewHospitalized: 0,
    NewRecovered: 0,
    Recovered: 0,
    UpdateDate: "",
  });

  useEffect(() => {
    const getToday = async () => {
      // const { data } = await todayService();
      const service = new TodayService();
      const { data } = await service.getAll();
      if (typeof data != "undefined") {
        setData(data);
      }
    };
    getToday();
  }, []);

  const openWebpage = () => {
    Linking.openURL("http://covid19.ddc.moph.go.th/th/self_screening");
  };
  return (
    <>
      {/* <StatusBar backgroundColor="#e1298e" /> */}
      <View style={styles.container}>
        <View>
          <Text
            style={[styles.center, styles.h1, styles.cGreen, { marginTop: 20 }]}
          >
            รายงานสถานการณ์ โควิด-19
          </Text>
          <Text style={[styles.center, styles.cGreen, { marginBottom: 20 }]}>
            อัพเดทข้อมูลล่าสุด : {data.UpdateDate}
          </Text>
        </View>
        <View style={[styles.fixToText, { marginBottom: 20 }]}>
          <Button
            title="ทดสอบแบบประเมินความเสี่ยง"
            color={Colors.primary}
            onPress={openWebpage}
          />
        </View>
        <View>
          <BoxGroup>
            <CovidCard
              navigation={navigation}
              color={Colors.primary}
              title="ติดเชื้อสะสม"
              amount={data.Confirmed}
              increase={data.NewConfirmed}
            ></CovidCard>
          </BoxGroup>
          <BoxGroup>
            <CovidCard
              navigation={navigation}
              color={Colors.success}
              title="หายแล้ว"
              amount={data.Recovered}
              increase={data.NewRecovered}
            ></CovidCard>
            <CovidCard
              navigation={navigation}
              color={Colors.info}
              title="รักษาอยู่ใน รพ."
              amount={data.Hospitalized}
              increase={data.NewHospitalized}
            ></CovidCard>
            <CovidCard
              navigation={navigation}
              color={Colors.gray}
              title="เสียชีวิต"
              amount={data.Deaths}
              increase={data.NewDeaths}
            ></CovidCard>
          </BoxGroup>
        </View>

        <View style={{ flexDirection: "row", justifyContent: "center" }}>
          <Button
            title="History"
            onPress={() => {
              navigation.push("History", { param: "test" });
            }}
          ></Button>
        </View>
        <View style={{ flexDirection: "row", justifyContent: "center", marginTop: 10 }}>
          <Button
            title="TodoRedux"
            onPress={() => {
              navigation.push("TodoRedux");
            }}
          ></Button>
        </View>
        <View>
          <Toggle />
        </View>
      </View>
    </>
  );
};
export default HomeScreen;

HomeScreen.navigationOptions = ({ navigation }) => ({
  title: "รายงานสถานการณ์ โควิด-19",
  headerStyle: {
    backgroundColor: Colors.primary,
  },
  headerTintColor: "#FFFFFF",
  headerTitleStyle: { color: "#fff" },
  headerBackTitle: "",
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  center: {
    textAlign: "center",
  },
  h1: {
    fontSize: 30,
  },
  cGreen: {
    color: "#006738",
  },
  fixToText: {
    flexDirection: "row",
    justifyContent: "center",
  },
  bPink: {
    backgroundColor: "#e1298e",
  },
  bGreen: {
    backgroundColor: "#006738",
  },
  bGreenBlue: {
    backgroundColor: "#179c9b",
  },
  bGray: {
    backgroundColor: "#666",
  },
  box: {
    borderRadius: 10,
    margin: 5,
  },
  boxText: {
    textAlign: "center",
  },
});
