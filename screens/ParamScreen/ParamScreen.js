import React from "react";
import { View, Text } from "react-native";

const TodoScreen = ({ navigation }) => {
  console.log(navigation);
  const {state: {params: { amount, increase, title }} } = navigation;
  return (
    <View>
      <Text>{title}</Text>
      <Text>{amount}</Text>
      <Text>{increase}</Text> 
    </View>
  );
};
export default TodoScreen;
