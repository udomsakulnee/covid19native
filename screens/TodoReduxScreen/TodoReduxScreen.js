import React, { useState } from "react";
import TodoForm from "../../components/TodoForm/TodoForm";
import TodoList from "../../components/TodoList/TodoList";

const TodoReduxScreen = ({ navigation }) => {
  const [todo, setTodo] = useState();
  return (
    <>
      <TodoForm></TodoForm>
      <TodoList></TodoList>
    </>
  );
};
export default TodoReduxScreen;
