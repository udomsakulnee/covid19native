import React, { useState, useEffect } from 'react';
import { Image, ScrollView, View } from 'react-native';
import { List } from '@ant-design/react-native';
import Colors from "../../styles/colors";
// import { caseSumService } from "../../services/stat-covid-19";
import { CaseSumService } from "../../services";
const Item = List.Item;
const Brief = Item.Brief;

const HistoryScreen = ({ navigation }) => {
  const [provinces, setProvinces] = useState([]);

  useEffect(() => {
    const caseSum = async () => {
      const service = new CaseSumService;
      const { data, status } = await service.getAll();
      // const { data, status } = await caseSumService();

      if (status == 200) {
        // console.info(data);
        let objectToArray = [];
        for (let key in data.Province) {
          objectToArray = [
            ...objectToArray,
            { province: key, amount: data.Province[key] },
          ];
        }

        setProvinces(objectToArray);
      }
    };
    caseSum();
  }, []);

  return (
    <ScrollView
      style={{ flex: 1, backgroundColor: '#f5f5f9' }}
    >
      <List renderHeader={'รายงานสถานการณ์ โควิด-19'}>
        {provinces.map(({ province, amount }) =>
          <Item>
            {province + " " + amount}
          </Item>
        )}
      </List>
    </ScrollView>
  );
};
export default HistoryScreen;

HistoryScreen.navigationOptions = ({ navigation }) => ({
  title: "รายงานสถานการณ์ โควิด-19",
  headerStyle: {
    backgroundColor: Colors.primary,
  },
  headerTintColor: "#FFFFFF",
  headerTitleStyle: { color: "#fff" },
  headerBackTitle: "",
});
