import React, { useState } from "react";
import { View, Button } from "react-native";
import Colors from "../../styles/colors";

const LoginScreen = ({ navigation }) => {
  return (
    <View style={{ flex: 1, justifyContent: "center", alignItems: "center", backgroundColor: Colors.primary }}>
      <View style={{ width: 100 }}>
        <Button title="Login" onPress={() => { navigation.navigate("App") }}></Button>
      </View>
    </View>
  );
};
export default LoginScreen;

LoginScreen.navigationOptions = ({ navigation }) => ({
  title: "Login",
  headerStyle: {
    backgroundColor: "#119CED",
  },
  headerTintColor: "#FFFFFF",
  headerTitleStyle: { color: "#fff" },
  headerBackTitle: " ",
});
