import Service from "@wiseidea/util-service";

export class TodayService extends Service {
  constructor() {
    super("https://covid19.th-stat.com/api/open/today");
  }
}
