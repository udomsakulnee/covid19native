import Service from "@wiseidea/util-service";

export class CaseSumService extends Service {
  constructor() {
    super("https://covid19.th-stat.com/api/open/cases/sum");
  }
}
