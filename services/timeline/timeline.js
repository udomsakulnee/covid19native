import Service from "@wiseidea/util-service";

export class TimelineService extends Service {
  constructor() {
    super("https://covid19.th-stat.com/api/open/timeline");
  }
}