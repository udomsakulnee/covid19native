import React from "react";
import { Image } from "react-native";

import LoginScreen from "../screens/LoginScreen/LoginScreen";
import HomeScreen from "../screens/HomeScreen/HomeScreen";
import HistoryScreen from "../screens/HistoryScreen/HistoryScreen";
import ParamScreen from "../screens/ParamScreen/ParamScreen";
import TodoReduxScreen from "../screens/TodoReduxScreen/TodoReduxScreen";

import { createSwitchNavigator, createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import { createBottomTabNavigator } from "react-navigation-tabs";

// const TabStack = createBottomTabNavigator({
//   JSONFeed: {
//     screen: JSONFeedScreen,
//     navigationOptions: {
//       tabBarLabel: "Feed",
//       tabBarIcon: ({ focused }) => (
//         <Image
//           style={{
//             height: 28,
//             width: 28,
//           }}
//           resizeMode="contain"
//           source={
//             focused
//               ? require("../assets/img/ic_profile_select.png")
//               : require("../assets/img/ic_profile.png")
//           }
//         />
//       ),
//     },
//   },
//   Camera: {
//     screen: CameraScreen,
//     navigationOptions: {
//       tabBarLabel: "Camera",
//       tabBarIcon: ({ focused }) => (
//         <Image
//           style={{
//             height: 28,
//             width: 28,
//           }}
//           resizeMode="contain"
//           source={
//             focused
//               ? require("../assets/img/ic_card_select.png")
//               : require("../assets/img/ic_card.png")
//           }
//         />
//       ),
//     },
//   },
// });

// TabStack.navigationOptions = ({ navigation }) => {
//   const { routeName } = navigation.state.routes[navigation.state.index];

//   // You can do whatever you like here to pick the title based on the route name
//   const headerTitle = routeName;

//   return {
//     headerTitle,
//     headerStyle: { backgroundColor: "#339CED" },
//     headerTitleStyle: { color: "#fff" },
//   };
// };

const AppStack = createStackNavigator(
  {
    Home: { screen: HomeScreen },
    History: { screen: HistoryScreen },
    Param: { screen: ParamScreen },
    TodoRedux: { screen: TodoReduxScreen }
  },
  {
    initialRouteName: "Home",
  }
);

export default createAppContainer(
  createSwitchNavigator(
    {
      Login: { screen: LoginScreen },
      App: AppStack,
    },
    {
      initialRouteName: "Login",
    }
  )
);
