import moment from "moment";
import "moment/locale/th";

export const commaFormat = (num) => {
  if (typeof num != "undefined" && num != null) {
    return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  } else {
    return null;
  }
};

export const dateFormat = (date) => {
  moment.locale("th");
  if (typeof date != "undefined" && date != null) {
    return (
      moment(date, "DD/MM/YYYY").format("D MMMM ") + (Number(moment(date, "DD/MM/YYYY").format("Y")) + 543)
    );
  }
};
